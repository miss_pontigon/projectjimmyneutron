﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.picLogoheader1 = New System.Windows.Forms.PictureBox()
        Me.lblHeadername1 = New System.Windows.Forms.Label()
        Me.lblHeaderadd1 = New System.Windows.Forms.Label()
        Me.picAdmin1 = New System.Windows.Forms.PictureBox()
        Me.lblAdmin1 = New System.Windows.Forms.Label()
        Me.txtPassword1 = New System.Windows.Forms.TextBox()
        Me.btnLogin1 = New System.Windows.Forms.Button()
        CType(Me.picLogoheader1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAdmin1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picLogoheader1
        '
        Me.picLogoheader1.Image = CType(resources.GetObject("picLogoheader1.Image"), System.Drawing.Image)
        Me.picLogoheader1.Location = New System.Drawing.Point(79, 28)
        Me.picLogoheader1.Name = "picLogoheader1"
        Me.picLogoheader1.Size = New System.Drawing.Size(135, 64)
        Me.picLogoheader1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLogoheader1.TabIndex = 0
        Me.picLogoheader1.TabStop = False
        '
        'lblHeadername1
        '
        Me.lblHeadername1.Font = New System.Drawing.Font("Lucida Sans Unicode", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadername1.Location = New System.Drawing.Point(220, 38)
        Me.lblHeadername1.Name = "lblHeadername1"
        Me.lblHeadername1.Size = New System.Drawing.Size(337, 27)
        Me.lblHeadername1.TabIndex = 1
        Me.lblHeadername1.Text = "Barangay Sangang Daan"
        Me.lblHeadername1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblHeaderadd1
        '
        Me.lblHeaderadd1.Font = New System.Drawing.Font("Lucida Sans Unicode", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderadd1.Location = New System.Drawing.Point(220, 65)
        Me.lblHeaderadd1.Name = "lblHeaderadd1"
        Me.lblHeaderadd1.Size = New System.Drawing.Size(337, 27)
        Me.lblHeaderadd1.TabIndex = 2
        Me.lblHeaderadd1.Text = "Project 8 Quezon City, Philippines"
        Me.lblHeaderadd1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'picAdmin1
        '
        Me.picAdmin1.Image = CType(resources.GetObject("picAdmin1.Image"), System.Drawing.Image)
        Me.picAdmin1.Location = New System.Drawing.Point(189, 164)
        Me.picAdmin1.Name = "picAdmin1"
        Me.picAdmin1.Size = New System.Drawing.Size(109, 101)
        Me.picAdmin1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picAdmin1.TabIndex = 3
        Me.picAdmin1.TabStop = False
        '
        'lblAdmin1
        '
        Me.lblAdmin1.Font = New System.Drawing.Font("Lucida Sans Unicode", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdmin1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblAdmin1.Location = New System.Drawing.Point(304, 176)
        Me.lblAdmin1.Name = "lblAdmin1"
        Me.lblAdmin1.Size = New System.Drawing.Size(103, 33)
        Me.lblAdmin1.TabIndex = 4
        Me.lblAdmin1.Text = "Admin"
        '
        'txtPassword1
        '
        Me.txtPassword1.Font = New System.Drawing.Font("Lucida Sans Unicode", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.txtPassword1.Location = New System.Drawing.Point(308, 212)
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword1.Size = New System.Drawing.Size(161, 24)
        Me.txtPassword1.TabIndex = 5
        '
        'btnLogin1
        '
        Me.btnLogin1.Font = New System.Drawing.Font("Lucida Sans Unicode", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin1.Location = New System.Drawing.Point(308, 242)
        Me.btnLogin1.Name = "btnLogin1"
        Me.btnLogin1.Size = New System.Drawing.Size(75, 23)
        Me.btnLogin1.TabIndex = 6
        Me.btnLogin1.Text = "Log In"
        Me.btnLogin1.UseVisualStyleBackColor = True
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 361)
        Me.Controls.Add(Me.btnLogin1)
        Me.Controls.Add(Me.txtPassword1)
        Me.Controls.Add(Me.lblAdmin1)
        Me.Controls.Add(Me.picAdmin1)
        Me.Controls.Add(Me.lblHeaderadd1)
        Me.Controls.Add(Me.lblHeadername1)
        Me.Controls.Add(Me.picLogoheader1)
        Me.Font = New System.Drawing.Font("Lucida Sans Unicode", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Barangay Registration System"
        CType(Me.picLogoheader1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAdmin1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picLogoheader1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblHeadername1 As System.Windows.Forms.Label
    Friend WithEvents lblHeaderadd1 As System.Windows.Forms.Label
    Friend WithEvents picAdmin1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblAdmin1 As System.Windows.Forms.Label
    Friend WithEvents txtPassword1 As System.Windows.Forms.TextBox
    Friend WithEvents btnLogin1 As System.Windows.Forms.Button

End Class
